#!/bin/bash

PID=/tmp/goapp.pid

# First of all cd into our project directory.
cd $APP_DIR || exit
# Get the current path
CURPATH=`pwd`

# Before listening to changes to files try to build
# and run the app at startup.
go build || true
$APP_DIR/$APP_NAME & echo $! > $PID

inotifywait -mr --timefmt '%d/%m/%y %H:%M' --format '%T %w %f %e' \
-e close_write -e delete -e modify -e move $APP_DIR | while read date time dir file event; do

  # First of all make sure the changed file ends with .go extension
  # if we are to rebuild and restart the go app.
  echo "At ${time} on ${date}, the event(s) ${event} occured for file ${file}" >> /var/log/gowatch.log
  echo >> /var/log/gowatch.log
  if [[ $file =~ \.go$ ]]; then
    # Now attempt to rebuild the app.
    # On failure simply exit with a zero code to basically say
    # "I will only build when the code will compile correctly"
    echo "Now attempting to rebuild app due to the ${event} event on ${file}" >> /var/log/gowatch.log
    # Do an empty echo to ensure next echo will be to a new line.
    echo >> /var/log/gowatch.log
    if go build ; then
      # Only kill the current process for the app if we were able to rebuild.
      kill "`cat $PID`" || true
      # Now re-run the app.
      $APP_DIR/$APP_NAME & echo $! > $PID
    fi
  fi
done
